import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { getData } from '../request/request'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        allData:[],
        allCount:0,
        allMoney:0,
        shopCar:[],
        carMoney:0
    },
    mutations:{
        async haveData(state,data){
            state.allData = data.data.categoryList
        },
        addCounts(state,payload){
            if(payload.type === 'add'){
                state.allCount ++
                state.allData.forEach((v,i)=>{
                    v.spuList.forEach((itm,ind)=>{
                       if(itm.spuId===payload.id){
                        if(!itm.count){
                            itm.count = 1
                        }else{
                            itm.count ++
                        }
                       }
                    })
                })
            }else{
                state.allCount --
                state.allData.forEach((v,i)=>{
                    v.spuList.forEach((itm,ind)=>{
                        if(itm.spuId===payload.id){
                           itm.count--
                        }
                    })
                })
                
            }
          
            state.allData = [...state.allData]
        },
        pushItm(state,item){
           let shop = state.shopCar.filter((v,i)=>{
               return v.spuId === item.spuId
           })
           if(shop.length === 1){
                item.count+1
           }else{
               state.shopCar.push(item)
           }
           console.log(state.shopCar)
           state.allMoney=0
           state.shopCar = state.shopCar.filter((v,i)=>{
               return v.count>0
           })
           state.shopCar.forEach((v,i)=>{
               state.allMoney += v.count * v.originPrice;
               state.carMoney += v.count * v.originPrice;
           })
          
        }
    },
    actions:{
        async getData(context){
        context.commit('haveData',await getData())
        },
        addCount(content,payload){
            content.commit('addCounts',payload)
            content.commit('pushItm',payload.item)
        }
    }
})