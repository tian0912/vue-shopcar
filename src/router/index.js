import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Shops from '../view/shops'
import Shoper from '../view/shoper'
import Good from '../view/good'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
     redirect:'/shops'
    },
    {
      path:'/shops',
      name:'shops',
      component:Shops
    },{
      path:'/good',
      name:'good',
      component:Good
    },{
      path:'/shoper',
      name:'shoper',
      component:Shoper
    }

  ]
})
